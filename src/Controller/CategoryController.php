<?php

namespace App\Controller;

use App\Entity\Category;

use App\Form\CategoryType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CategoryController extends AbstractController
{

    //security : add with login
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }



    
    /**
     * @Route("/category", name="category")
     */
    public function index(CategoryRepository $CategoryRepo): Response
    {

        $categories = $CategoryRepo->findAll();

        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'categories'=>$categories
        ]);
    }


/**
     * @Route("/newcategory", name="category.add", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function new(Request $request) : Response
    {


        $category = new Category();

        $category->setName($this->security->getUser());
        
        //$category->setCreatedAt(new \DateTime());
        

        $form = $this->createForm(categoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash('success','la category a bien ete ajoute');
            return $this->redirectToRoute('category');
        }

        return $this->render('category/new.html.twig',[
            'category'=>$category,
            'form'=>$form->createView()
        ]);
        
    }

    /**
     * @Route("/{id}/category",name="category.edit",methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, category $category) : Response
    {

        //$category->setUpdatedAt(new \DateTime());

        $form = $this->createForm(categoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','La modification de la category a bien ete enregistre');
            return $this->redirectToRoute('category');
        }
        return $this->render('category/edit.html.twig',[
            'category'=>$category,
            'form'=>$form->createView()
        ]);
    }
    
    /**
     * @Route("/delete/{id}",name="category.delete",methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Category $category) : Response
    {
        if($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();

            $this->addFlash('success','La category a bien ete supprime');
        }
        return $this->redirectToRoute('category');
    }

}
