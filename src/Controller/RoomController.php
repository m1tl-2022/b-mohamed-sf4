<?php

namespace App\Controller;

use App\Entity\Room;

use App\Form\RoomType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\RoomRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class RoomController extends AbstractController
{
    //security : add with login
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }



    /**
     * @Route("/room", name="room")
     */
    public function index(RoomRepository $RoomRepo, CategoryRepository $CategoryRepo): Response
    {

        $rooms = $RoomRepo->findAvailableRoom();
        $categories = $CategoryRepo->findAll();

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms'=>$rooms,
            'categories'=>$categories
        ]);
    }

    /**
     * @Route("/new", name="room.add", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function new(Request $request) : Response
    {


        $room = new Room();

        $room->setUser($this->security->getUser());
        
        $room->setCreatedAt(new \DateTime());
        

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('success','la salle a bien ete ajoute');
            return $this->redirectToRoute('room');
        }

        return $this->render('room/new.html.twig',[
            'room'=>$room,
            'form'=>$form->createView()
        ]);
        
    }

    /**
     * @Route("/{id}/edit",name="room.edit",methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Room $room) : Response
    {

        $room->setUpdatedAt(new \DateTime());

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','La modification de la salle a bien ete enregistre');
            return $this->redirectToRoute('room');
        }
        return $this->render('room/edit.html.twig',[
            'room'=>$room,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}",name="room.delete",methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Room $room) : Response
    {
        if($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success','La salle a bien ete supprime');
        }
        return $this->redirectToRoute('room');
    }
}


